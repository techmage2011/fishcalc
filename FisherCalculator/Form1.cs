﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq.Expressions;

namespace FisherCalculator
{
    public partial class MainForm : Form
    {
        private String buffer = "";
        private String operation = "";

        public MainForm()
        {
            InitializeComponent();
        }

        private void DisplayAdd(String addChar)
        {
            Display.Text = Display.Text + addChar;
        }

        private void One_Click(object sender, EventArgs e)
        {
            DisplayAdd("1");
        }

        private void Two_Click(object sender, EventArgs e)
        {
            DisplayAdd("2");
        }

        private void Three_Click(object sender, EventArgs e)
        {
            DisplayAdd("3");
        }

        private void Four_Click(object sender, EventArgs e)
        {
            DisplayAdd("4");
        }

        private void Five_Click(object sender, EventArgs e)
        {
            DisplayAdd("5");
        }

        private void Six_Click(object sender, EventArgs e)
        {
            DisplayAdd("6");
        }

        private void Seven_Click(object sender, EventArgs e)
        {
            DisplayAdd("7");
        }

        private void Eight_Click(object sender, EventArgs e)
        {
            DisplayAdd("8");
        }

        private void Nine_Click(object sender, EventArgs e)
        {
            DisplayAdd("9");
        }

        private void Zero_Click(object sender, EventArgs e)
        {
            DisplayAdd("0");
        }

        private void Dot_Click(object sender, EventArgs e)
        {
            DisplayAdd(".");
        }

        private void NegativeButton_Click(object sender, EventArgs e)
        {
            DisplayAdd("-");
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            Clear_Display();
        }

        private void AllClear_Click(object sender, EventArgs e)
        {
            All_Clear();
        }

        private void Clear_Display()
        {
            Display.Text = "";
        }

        private void All_Clear()
        {
            Display.Text = "";
            buffer = "";
            operation = "";
            StatusLabel.Text = "";
        }

        private void Equals_Click(object sender, EventArgs e)
        {
            if (!buffer.Equals("") && Display.Text!=null && !Display.Text.Equals("") && !operation.Equals(""))
            {
                switch (operation)
                {
                    case "/":
                        Display.Text = (System.Convert.ToDecimal(buffer) / System.Convert.ToDecimal(Display.Text)).ToString();
                        break;
                    case "*":
                        Display.Text = (System.Convert.ToDecimal(Display.Text) * System.Convert.ToDecimal(buffer)).ToString();
                        break;
                    case "-":
                        Display.Text = (System.Convert.ToDecimal(buffer) - System.Convert.ToDecimal(Display.Text)).ToString();
                        break;
                    case "+":
                        Display.Text = (System.Convert.ToDecimal(Display.Text) + System.Convert.ToDecimal(buffer)).ToString();
                        break;
                }
            }
            buffer = "";
            operation = "";
            StatusLabel.Text = "";
        }

        private void Store_Buffer()
        {
            if (buffer.Equals(""))
            {
                buffer = Display.Text;
                StatusLabel.Text = buffer + " " + operation;
            }

            Clear_Display();
        }

        private void Subtract_Click(object sender, EventArgs e)
        {
            operation = "-";
            Store_Buffer();
        }

        private void Addition_Click(object sender, EventArgs e)
        {
            operation = "+";
            Store_Buffer();
        }

        private void Multiply_Click(object sender, EventArgs e)
        {
            operation = "*";
            Store_Buffer();
        }

        private void Divide_Click(object sender, EventArgs e)
        {
            operation = "/";
            Store_Buffer();
        }

    }
}
